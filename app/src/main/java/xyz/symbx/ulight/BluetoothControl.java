package xyz.symbx.ulight;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.util.Log;

import java.util.List;

public class BluetoothControl {
    private final BluetoothGattCallback _callback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            Log.d("SymbX", String.format("Connect %d -> %d", status, newState));
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    Log.d("SymbX", "Connected");
                    gatt.discoverServices();
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    Log.d("SymbX", "Disconnected");
                    gatt.close();
                } else {
                    Log.d("SymbX", "Another state?");
                }
            } else {
                Log.d("SymbX", "Fail 1");
                gatt.close();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            List<BluetoothGattService> services = gatt.getServices();
            for (BluetoothGattService service : services) {
                if (!service.getUuid().toString().equals(Constants.SERVICE_UUID)) {
                    continue;
                }
                Log.d("SymbX", "Service: " + service.getUuid().toString());
                for (BluetoothGattCharacteristic chr : service.getCharacteristics()) {
                    if (!chr.getUuid().toString().equals(Constants.CHAR_UUID)) {
                        continue;
                    }
                    Log.d("SymbX", "Char: " + chr.getUuid().toString());
                    int props = chr.getProperties();
                    if ((props & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                        gatt.setCharacteristicNotification(chr, true);
                        chr.setValue(_packet);
                        gatt.writeCharacteristic(chr);
                    }
                }
            }
            gatt.disconnect();
            gatt.close();
        }
    };
    private String _packet = null;
    private BluetoothDevice _device = null;

    public BluetoothControl(BluetoothDevice dev, LightCommand cmd) {
        format(cmd);
        _device = dev;
    }

    private void format(LightCommand cmd) {
        switch (cmd) {
            case On:
                _packet = "$+";
                break;
            case Off:
                _packet = "$,";
                break;
            case Keep:
                _packet = "$*";
                break;
        }
    }

    public void send() {
        _device.connectGatt(null, false, _callback);
    }
}
