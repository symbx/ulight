package xyz.symbx.ulight;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class MainActivityViewModel extends AndroidViewModel {

    private BluetoothAdapter _adapter;
    private List<BluetoothDevice> _devices = new ArrayList<>();
    private MutableLiveData<Collection<BluetoothDevice>> _devicesData = new MutableLiveData<>();
    private ScanSettings _settings;
    private ScanFilter _filter;
    private boolean _setup = false;
    private boolean _scanning = false;
    private final ScanCallback _callback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            BluetoothDevice dev = result.getDevice();
            // if my? filter?
            for (BluetoothDevice device : _devices) {
                // Log.d("SymbX", device.getAddress() + " / " + dev.getAddress());
                if (device.getAddress().equals(dev.getAddress())) {
                    return;
                }
            }
            _devices.add(dev);
            _devicesData.postValue(_devices);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            _devices.clear();
            for (ScanResult res : results) {
                _devices.add(res.getDevice());
            }
            _devicesData.postValue(_devices);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.d("SymbX", "Scan fail");
        }
    };
    private BluetoothLeScanner _scanner;

    public MainActivityViewModel(@NonNull Application app) {
        super(app);
    }

    public boolean setupViewModel(Activity activity) {
        if (!_setup) {
            _setup = true;
            _adapter = ((BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
            if (_adapter == null) {
                Toast.makeText(getApplication(), R.string.no_bluetooth, Toast.LENGTH_LONG).show();
                return false;
            }
            _scanner = _adapter.getBluetoothLeScanner();
            _settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            _filter = new ScanFilter.Builder()
                    .setServiceUuid(ParcelUuid.fromString(Constants.SERVICE_UUID))
                    .build();
        }
        return true;
    }

    @SuppressLint("MissingPermission")
    public void refreshPairedDevices() {
        if (!_scanning) {
            _scanner.startScan(Collections.singletonList(_filter), _settings, _callback);
            _scanning = true;
        }
    }

    @Override
    protected void onCleared() {
        this.stopScan();
    }

    @SuppressLint("MissingPermission")
    public void stopScan() {
        if (_scanning) {
            _scanner.stopScan(_callback);
        }
    }

    public LiveData<Collection<BluetoothDevice>> getPairedDeviceList() {
        return _devicesData;
    }

    public boolean isBluetoothEnabled() {
        if (_adapter == null) {
            return false;
        }
        return _adapter.isEnabled();
    }
}
