package xyz.symbx.ulight;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;

import java.util.Set;

public class TimerBroadcaster extends BroadcastReceiver {

    private final String lightTick = "LightTick";

    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "YOUR TAG");
        //Acquire the lock
        wl.acquire(60 * 1000L /*1 minutes*/);

        Bundle extras = intent.getExtras();

        if (extras != null && extras.getBoolean(lightTick, Boolean.FALSE)){
            SharedPreferences prefs = context.getSharedPreferences("ulight", Context.MODE_MULTI_PROCESS);
            Set<String> macs = prefs.getStringSet("devs", null);
            if (macs != null && macs.size() > 0) {
                BluetoothAdapter adapter = ((BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
                for (String mac : macs) {
                    BluetoothDevice dev = adapter.getRemoteDevice(mac);
                    new BluetoothControl(dev, LightCommand.Keep).send();
                }
            }
        }

        //Release the lock
        wl.release();
    }

    public void SetAlarm(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, TimerBroadcaster.class);
        intent.putExtra(lightTick, Boolean.FALSE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_IMMUTABLE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 2 * 60 *1000 , pi);
    }

    public void CancelAlarm(Context context)
    {
        Intent intent = new Intent(context, TimerBroadcaster.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}
