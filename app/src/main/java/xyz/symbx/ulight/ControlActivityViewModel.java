package xyz.symbx.ulight;

import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.HashSet;
import java.util.Set;

public class ControlActivityViewModel extends AndroidViewModel {

    private BluetoothAdapter _adapter;
    private final MutableLiveData<String> _deviceNameData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> _keepData = new MutableLiveData<>();
    private String _mac;
    private boolean _setup = false;

    public ControlActivityViewModel(@NonNull Application app) {
        super(app);
    }

    public boolean setupViewModel(Activity activity, String deviceName, String mac) {
        if (!_setup) {
            _setup = true;
            _adapter = ((BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
            this._mac = mac;
            _deviceNameData.postValue(deviceName);

            SharedPreferences prefs = activity.getSharedPreferences("ulight", Context.MODE_MULTI_PROCESS);
            Set<String> macs = prefs.getStringSet("devs", null);
            if (macs != null && macs.contains(mac)) {
                _keepData.postValue(true);
            } else {
                _keepData.postValue(false);
            }
        }
        return true;
    }

    public LiveData<String> getDeviceName() {
        return _deviceNameData;
    }

    public LiveData<Boolean> getKeep() {
        return _keepData;
    }

    public void sendCommand(LightCommand cmd) {
        BluetoothDevice device = _adapter.getRemoteDevice(_mac);
        new BluetoothControl(device, cmd).send();
    }

    public void toggleKeep(Activity activity) {
        SharedPreferences prefs = activity.getSharedPreferences("ulight", Context.MODE_MULTI_PROCESS);
        Set<String> macs = prefs.getStringSet("devs", null);
        _keepData.postValue(false);
        Set<String> res = new HashSet<>();
        if (macs == null) {
            macs = new HashSet<>();
        }
        for (String v : macs) {
            if (!v.equals(_mac)) {
                res.add(v);
            }
        }
        if (!macs.contains(_mac)) {
            res.add(_mac);
            _keepData.postValue(true);
        } else {
            _keepData.postValue(false);
        }
        SharedPreferences.Editor edit = prefs.edit();
        edit.putStringSet("devs", res);
        edit.apply();
    }
}
