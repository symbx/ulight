package xyz.symbx.ulight;

public enum LightCommand {
    On,
    Off,
    Keep
}
