package xyz.symbx.ulight;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.Set;

public class ControlActivity extends AppCompatActivity {

    private ControlActivityViewModel _model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        _model = ViewModelProviders.of(this).get(ControlActivityViewModel.class);
        if (!_model.setupViewModel(this, getIntent().getStringExtra("device_name"), getIntent().getStringExtra("device_mac"))) {
            finish();
            return;
        }
        Button keepBtn = findViewById(R.id.buttonKeep);
        _model.getDeviceName().observe(this, name -> setTitle(getString(R.string.device_name_format, name)));
        _model.getKeep().observe(this, state -> keepBtn.setText(getString(R.string.keepNearby, getString(state ? R.string.on : R.string.off))));

        findViewById(R.id.buttonOn).setOnClickListener(view -> _model.sendCommand(LightCommand.On));
        findViewById(R.id.buttonOff).setOnClickListener(view -> _model.sendCommand(LightCommand.Off));
        keepBtn.setOnClickListener(view -> _model.toggleKeep(this));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void addToKeep() {

    }
}