#ifndef STM_BASE_H
#define STM_BASE_H

#include <type.h>

// Enable interrupts
#define rim()		{__asm__("rim\n");}

// Disable interrupts
#define sim()		{__asm__("sim\n");}

// Do nothing
#define nop()		{__asm__("nop\n");}

// Trap (Soft IT)
#define trap()		{__asm__("trap\n");}

// Wait for interrupt
#define wfi()		{__asm__("wfi\n");}

// Halt
#define halt()		{__asm__("halt\n");}


// Interrupt
#define interrupt(key, name)		void name(void) __interrupt(key)

// Pins structure (easy access to each pin)
typedef struct {
	u08 Pin0	:1;
	u08 Pin1	:1;
	u08 Pin2	:1;
	u08 Pin3	:1;
	u08 Pin4	:1;
	u08 Pin5	:1;
	u08 Pin6	:1;
	u08 Pin7	:1;
} Pins;

// Port structure (all pins control)
typedef struct {
	Pins ODR;
	Pins IDR;
	Pins DDR;
	Pins CR1;
	Pins CR2;
} Port;

// Uart register structure
typedef struct {
	u08 Status	:8;
	u08 Data	:8;
	u08 BRR1	:8;
	u08 BRR2	:8;
	u08 CR1		:8;
	u08 CR2		:8;
	u08 CR3		:8;
	u08 CR4		:8;
	u08 CR5		:8;
	u08 GTR		:8;
	u08 PSCR	:8;
}  UART;

// Uart configuration bits
#define UART_TX_EMPTY				(1 << 7)
#define UART_TX_COMPLETE			(1 << 6)
#define UART_RX_NOT_EMPTY			(1 << 5)
#define UART_RX_IDLE				(1 << 4)
#define UART_RX_OVERRUN				(1 << 3)
#define UART_RX_NOISE				(1 << 2)
#define UART_RX_FRAME_ERROR			(1 << 1)
#define UART_RX_PARITY_ERROR		(1 << 0)

#define UART_CR2_SBK                (1 << 0)
#define UART_CR2_RWU                (1 << 1)
#define UART_CR2_REN                (1 << 2)
#define UART_CR2_TEN                (1 << 3)
#define UART_CR2_IDLE_IVT			(1 << 4)
#define UART_CR2_RXNE_IVT			(1 << 5)
#define UART_CR2_TXC_IVT			(1 << 6)
#define UART_CR2_TXE_IVT			(1 << 7)

#define UART_CR3_LINEN				(1 << 6)
#define UART_CR3_STOP2              (1 << 5)
#define UART_CR3_STOP1              (1 << 4)
#define UART_CR3_CLKEN				(1 << 3)
#define UART_CR3_CLKPOL				(1 << 2)
#define UART_CR3_CLKPHA				(1 << 1)
#define UART_CR3_LBCL				(1 << 0)

// Delay function (in ticks)
static void delay_tick(unsigned int t)
{
    while(t--);
}

// GPIO Control register structure (1)
typedef struct {
	u08 PortA	:2;
	u08 PortB	:2;
	u08 PortC	:2;
	u08 PortD	:2;
} ExternalInterruptControlFull;

// GPIO Control register structure (2)
typedef struct {
	u08 PortE	:2;
} ExternalInterruptControlLim;

// GPIO interrupt mode
#define EXTI_FALLING_LOW		0x00
#define EXTI_RISING				0x01
#define EXTI_FALLING			0x02
#define EXTI_RISING_FALLING		0x03

// Flash control register struct (1)
typedef struct {
	u08 FixedTime	:1;
	u08 Interrupt	:1;
	u08 ActiveHaltPowerDown	:1;
	u08 HaltPowerDown	:1;
	u08				:4;
} FlashControl1;

// Flash control register struct (2)
typedef struct {
	u08 StandardBlockProgramming	:1;
	u08 							:3;
	u08 FastBlockProgramming		:1;
	u08 BlockErase					:1;
	u08 WordProgramming				:1;
	u08 WriteOptionBytes			:1;
} FlashControl2;

typedef struct {
	u08 UserBootAreaSize			:6;
	u08 							:2;
} FlashProtection;

// Flash status register struct
typedef struct {
	u08 AttemptWriteProtected		:1;
	u08 FlashUnlocked				:1;
	u08 EndOfOperation				:1;
	u08 DataUnlocked				:1;
	u08 							:2;
	u08 EndOfHV						:1;
	u08								:1;
} FlashStatus;

typedef struct {
	FlashControl1 Control1;
	FlashControl2 Control2;
	FlashControl2 ComplimentaryControl;
	FlashProtection Protection;
	FlashProtection NProtection;
	FlashStatus Status;
	u16								:16;
	u08 FlashProtectionKey			:8;
	u08								:8;
	u08 DataProtectionKey			:8;
} Flash;

#define FLASH_UNLOCK_KEY1		0x56
#define FLASH_UNLOCK_KEY2		0xAE

#define DATA_UNLOCK_KEY1		0xAE
#define DATA_UNLOCK_KEY2		0x56

#define _MEM_(mem_addr)         (*(volatile u08*)(mem_addr))


typedef struct {
	u08 CounterEnable				:1;
	u08 UpdateDisable				:1;
	u08 UpdateRequestSource			:1;
	u08 OnePulseMode				:1;
	u08 Direction					:1;
	u08 CenterAlignedMode			:1;
	u08 AutoReloadPreload			:1;
} TimerControl1;

typedef struct {
	u08 CaptureCountrolPreload		:1;
	u08								:1;
	u08 CaptureControlUpdate		:1;
	u08								:1;
	u08 MasterMode					:3;
	u08								:1;
} TimerControl2;

typedef struct {
	u08 Mode						:3;
	u08 							:1;
	u08 Trigger						:3;
	u08 MasterSlave					:1;
} TimerSlaveControl;


typedef struct {
	u08	Filter						:4;
	u08 Prescaler					:2;
	u08 ExternalClock				:1;
	u08 TriggerPolariry				:1;
} TimerExternalTrigger;

typedef struct {
	u08 UpdateInterrupt				:1;
	u08 Capture1Interrupt			:1;
	u08 Capture2Interrupt			:1;
	u08 Capture3Interrupt			:1;
	u08 Capture4Interrupt			:1;
	u08 CommutationInterrupt		:1;
	u08 TriggerInterrupt			:1;
	u08 BreakInterrupt				:1;
} TimerInterrupt;

typedef struct {
	u08 UpdateInterruptFlag			:1;
	u08 Capture1InterruptFlag		:1;
	u08 Capture2InterruptFlag		:1;
	u08 Capture3InterruptFlag		:1;
	u08 Capture4InterruptFlag		:1;
	u08 CommutationInterruptFlag	:1;
	u08 TriggerInterruptFlag		:1;
	u08 BreakInterruptFlag			:1;
} TimerStatus1;

typedef struct {
	u08								:1;
	u08 Capture1OvercaptureFlag		:1;
	u08 Capture2OvercaptureFlag		:1;
	u08 Capture3OvercaptureFlag		:1;
	u08 Capture4OvercaptureFlag		:1;	
	u08								:3;
} TimerStatus2;

typedef struct {
	u08 UpdateGeneration			:1;
	u08 Capture1Generation			:1;
	u08 Capture2Generation			:1;
	u08 Capture3Generation			:1;
	u08 Capture4Generation			:1;
	u08 CaptureUpdateGeneration		:1;
	u08 TriggerGeneration			:1;
	u08 BreakGeneration				:1;
} TimerEventGen;

typedef struct {
	u08 Enable1						:1;
	u08 Polarity1					:1;
	u08 ComplimentaryEnable1		:1;
	u08 ComplimentaryPolarity1		:1;
	u08 Enable2						:1;
	u08 Polarity2					:1;
	u08 ComplimentaryEnable2		:1;
	u08 ComplimentaryPolarity2		:1;
} TimerCaptureRegister1;

typedef struct {
	u08 Enable3						:1;
	u08 Polarity3					:1;
	u08 ComplimentaryEnable3		:1;
	u08 ComplimentaryPolarity3		:1;
	u08 Enable4						:1;
	u08 Polarity4					:1;
	u08 ComplimentaryEnable4		:1;
	u08 ComplimentaryPolarity4		:1;
} TimerCaptureRegister2;

typedef struct {
	TimerControl1 Control1;
	TimerControl2 Control2;
	TimerSlaveControl SlaveControl;
	TimerExternalTrigger ExternalTrigger;
	TimerInterrupt Interrupt;
	TimerStatus1 Status1;
	TimerStatus2 Status2;
	TimerEventGen EventGeneration;
	u08 CaptureMode1				:8;
	u08 CaptureMode2				:8;
	u08 CaptureMode3				:8;
	u08 CaptureMode4				:8;
	TimerCaptureRegister1 Capture12;
	TimerCaptureRegister2 Capture34;
	u08 CounterH					:8;
	u08	CounterL					:8;
	u08 PrescalerH					:8;
	u08 PrescalerL					:8;
	u08 AutoReloadH					:8;
	u08 AutoReloadL					:8;
} AdvancedTimer;

typedef struct {
	u08 Enable 						:1;
	u08 							:5;
	u08 GeneralCallEnable			:1;
	u08 NoStretch 					:1;
} I2CControl1;

typedef struct {
	u08 Start						:1;
	u08 Stop 						:1;
	u08 Acknowledge					:1;
	u08 AcknowledgePosition			:1;
	u08 							:3;
	u08 SoftwareReset				:1;
} I2CControl2;

typedef struct {
	u08 Address10Bit0				:1;
	u08 Address						:7;
} I2CAddressL;

typedef struct {
	u08								:1;
	u08 Address10Bit89				:2;
	u08 							:3;
	u08 AddressConfig				:1;
	u08 AddressMode					:1;
} I2CAddressH;

typedef struct {
	u08 StartBit					:1;
	u08 AddressSent					:1;
	u08 ByteTransferFinished		:1;
	u08 Address10Bit0				:1;
	u08 StopFlag					:1;
	u08 							:1;
	u08 ReceiveNotEmpty				:1;
	u08 TransmitEmpty				:1;
} I2CStatus1;

typedef struct {
	u08 BusError					:1;
	u08 ArbitrationLost				:1;
	u08 AcknowledgeFailure			:1;
	u08 Overrun 					:1;
	u08								:1;
	u08 WakeUp						:1;
	u08								:2;
} I2CStatus2;

typedef struct {
	u08 MasterMode					:1;
	u08 BusBusy						:1;
	u08 Transmitting				:1;
	u08								:1;
	u08 GeneralCallHeader			:1;
	u08								:2;
	u08 DualFlag					:1;
} I2CStatus3;

typedef struct {
	u08 Error						:1;
	u08 Event						:1;
	u08 Buffer						:1;
	u08								:5;
} I2CInterrupt;

typedef struct {
	u16	Clock						:12;
	u08								:2;
	u08 FastModeDuty				:1;
	u08 FastMode					:1;
} I2CClockControl;

typedef struct {
	u08 Time						:6;
	u08								:2;
} I2CRiseControl;

typedef struct {
	I2CControl1 Control1;
	I2CControl2 Control2;
	u08	Frequency					:6;
	u08								:2;
	I2CAddressL	AddressL;
	I2CAddressH AddressH;
	u08								:8;
	u08 Data						:8;
	I2CStatus1 Status1;
	I2CStatus2 Status2;
	I2CStatus3 Status3;
	I2CInterrupt Interrupt;
	I2CClockControl ClockControl;
	I2CRiseControl TimeRise;
	u08								:8; // Errors
} I2C;

typedef struct {
	u08 HighSpeedInternalEnable		:1;
	u08 HighSpeedInternalReady		:1;
	u08 FastWakeUp					:1;
	u08 LowSpeedInternalEnable		:1;
	u08 LowSpeedInternalReady		:1;
	u08 RegulatorPoweroff			:1;
	u08								:2;
} ClockInternal;

typedef struct {
	u08 HighSpeedExternalEnable		:1;
	u08 HighSpeedExternalReady		:1;
	u08								:6;
} ClockExternal;

typedef enum {
	HighSpeedExternal = 0xB4,
	LowSpeedInternal = 0xD2,
	HighSpeedInternal = 0xE1
} ClockMasterType;

typedef struct {
	u08 SwitchBusy					:1;
	u08 SwitchStartStop				:1;
	u08 SwitchInterrupt				:1;
	u08 SwitchInterruptFlag			:1;
	u08								:4;
} ClockSwitch;

typedef struct {
	u08 Prescaler					:3;
	u08 HighSpeedPrescaler			:2;
	u08								:3;
} ClockDivider;

typedef struct {
	u08 I2C							:1;
	u08 SPI 						:1;
	u08								:1;
	u08 Serial1						:1;
	u08 Timer4 						:1;
	u08 Timer2 						:1;
	u08 							:1;
	u08 Timer1						:1;
} ClockPeripheral1;

typedef struct {
	u08 							:2;
	u08 AutoWakeUp					:1;
	u08 AnalogDigitalConvertor		:1;
	u08								:4;
} ClockPeripheral2;

typedef struct {
	u08 Enable 						:1;
	u08 AuxiliaryConnected			:1;
	u08 DetectionInterrupt			:1;
	u08 Detection 					:1;
	u08								:4;
} ClockSecuritySystem;

typedef struct {
	u08 Enable						:1;
	u08 Selection					:4;
	u08 Ready						:1;
	u08 Busy						:1;
	u08								:1;
} ClockOutput;

typedef struct {
	u08 HighSpeedIntenalTrim		:4;
	u08								:4;
} ClockTrimming;

typedef struct {
	u08 Divider						:1;
	u08								:7;
} ClockSWIM;

typedef struct {
	ClockInternal Internal;
	ClockExternal External;
	u08								:8;
	ClockMasterType MasterStatus	:8;
	ClockMasterType MasterSwitch	:8;
	ClockSwitch SwitchControl;
	ClockDivider Divider;
	ClockPeripheral1 Peripheral1;
	ClockSecuritySystem SecuritySystem;
	ClockOutput Output;
	ClockPeripheral2 Peripheral2;
	u08								:8;
	ClockTrimming Trimming;
	ClockSWIM SWIM;
} Clock;

typedef struct {
	u08 Counter 					:7;
	u08 Activate					:1;
} WatchdogControl;

typedef struct {
	u08 Counter 					:7;
	u08 							:1;
} WatchdogWindow;

typedef struct {
	WatchdogControl Control;
	WatchdogWindow Window;
} Watchdog;

typedef struct {
	u08 High						:8;
	u08 Low							:8;
} ADCBuffer;

typedef struct {
	u08 Channel						:4;
	u08 AnalogWatchdogInterrupt		:1;
	u08 Interrupt					:1;
	u08 AnalogWatchdog 				:1;
	u08 EndOfConversion				:1;
} ADCControlStatus;

typedef struct {
	u08 ConverterOn					:1;
	u08 ContinousConversion			:1;
	u08								:2;
	u08 Prescaler					:3;
	u08 							:1;
} ADCConfig1;

typedef struct {
	u08 							:1;
	u08 ScanMode					:1;
	u08								:1;
	u08	Alignment					:1;
	u08 ExternalEventSelection		:2;
	u08 ExternalTrigger				:1;
	u08								:1;
} ADCConfig2;

typedef struct {
	u08 							:6;
	u08 OverrunFlag					:1;
	u08 DataBufferEnable			:1;
} ADCConfig3;

typedef struct {
	u08 High						:8;
	u08 Low							:8;
} ADCData;

typedef struct {
	u08 High						:8;
	u08 Low							:2;
	u08								:6;
} ADCThreshold;

typedef struct {
	ADCControlStatus ControlStatus;
	ADCConfig1 Configuration1;
	ADCConfig2 Configuration2;
	ADCConfig3 Configuration3;
	ADCData Data;
	ADCData SchmittDisable;
	ADCThreshold HighThreshold;
	ADCThreshold LowThreshold;
	u08								:8; // Watchdog
	u08								:8; // Watchdog
	u08								:8; // Watchdog
	u08								:8; // Watchdog
} ADC;

typedef struct {
	u08 Enable						:1;
	u08 UpdateDisable				:1;
	u08 UpdateRequestSource			:1;
	u08 OnePulseMode				:1;
	u08								:3;
	u08 AutoReload					:1;
} GeneralTimerControl1;

typedef struct {
	u08								:4;
	u08	MasterModeSelection			:3;
	u08								:1;
} GeneralTimerControl2;

typedef struct {
	u08 SlaveModeSelection			:3;
	u08								:1;
	u08 TriggerSelection			:3;
	u08 MasterSlaveMode				:1;
} GeneralTimerSlaveControl;

typedef struct {
	u08 UpdateInterrupt				:1;
	u08 Capture1Interrupt			:1;
	u08 Capture2Interrupt			:1;
	u08 Capture3Interrupt			:1;
	u08								:2;
	u08 TriggerInterrupt			:1;
	u08								:1;
} GeneralTimerInterrupt;

typedef struct {
	u08 UpdateInterruptFlag			:1;
	u08 Capture1InterruptFlag		:1;
	u08 Capture2InterruptFlag		:1;
	u08 Capture3InterruptFlag		:1;
	u08								:2;
	u08 TriggerInterruptFlag		:1;
	u08								:1;
} GeneralTimerStatus1;

typedef struct {
	u08 							:1;
	u08	Overcapture1Flag			:1;
	u08	Overcapture2Flag			:1;
	u08	Overcapture3Flag			:1;
	u08								:4;
} GeneralTimerStatus2;

typedef struct {
	u08 UpdateGeneration			:1;
	u08 Capture1Generation			:1;
	u08 Capture2Generation			:1;
	u08 Capture3Generation			:1;
	u08								:2;
	u08 TriggerGeneration			:1;
	u08								:1;
} GeneralTimerEventGeneration;

typedef struct {
	u08 CaptureSelection			:2;
	u08								:1;
	u08 PreloadEnable				:1;
	u08 CompareMode					:3;
	u08								:1;
} GeneralTimerCaptureMode;

typedef struct {
	u08 Capture1Enable				:1;
	u08 Capture1Polarity			:1;
	u08								:2;
	u08 Capture2Enable				:1;
	u08 Capture2Polarity			:1;
	u08								:2;
} GeneralTimerCaptureRegister1;

typedef struct {
	u08 Capture3Enable				:1;
	u08 Capture3Polarity			:1;
	u08								:6;
} GeneralTimerCaptureRegister2;

typedef struct {
	u08 High 						:8;
	u08 Low 						:8;
} GeneralTimer16Register;


typedef struct {
	GeneralTimerControl1 Control1;
	u08								:8;
	u08								:8;
	GeneralTimerInterrupt Interrupt;
	GeneralTimerStatus1 Status1;
	GeneralTimerStatus2 Status2;
	GeneralTimerEventGeneration Event;
	GeneralTimerCaptureMode Capture1;
	GeneralTimerCaptureMode Capture2;
	GeneralTimerCaptureMode Capture3;
	GeneralTimerCaptureRegister1 CaptureReg1;
	GeneralTimerCaptureRegister2 CaptureReg2;
	GeneralTimer16Register Counter;
	u08 Prescaler					:4;
	u08								:4;
	GeneralTimer16Register AutoReload;
	GeneralTimer16Register Compare1;
	GeneralTimer16Register Compare2;
	GeneralTimer16Register Compare3;
} GeneralTimer;

typedef enum {
	IndependWatchdogOff  = 0x00,
	IndependWatchdogEdit = 0x55,
	IndependWatchdogReset= 0xAA,
	IndependWatchdogOn   = 0xCC
} IndependWatchdogKey;


typedef enum {
    WatchdogPrescaler_4  = 0x00,
    WatchdogPrescaler_8  = 0x01,
    WatchdogPrescaler_16 = 0x02,
    WatchdogPrescaler_32 = 0x03,
    WatchdogPrescaler_64 = 0x04,
    WatchdogPrescaler_128= 0x05,
    WatchdogPrescaler_256= 0x06
} WatchdogPrescaler;

typedef struct {
    IndependWatchdogKey Key         :8;
    WatchdogPrescaler Prescaler     :3;
    u08                             :5;
    u08 Reload                      :8;
} IndependWatchdog;
#endif//STM_BASE_H