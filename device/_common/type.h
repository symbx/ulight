#ifndef STM_TYPE_H
#define STM_TYPE_H


// Here defined basic types used in code
typedef unsigned char	u08;
typedef signed char		i08;
typedef unsigned short	u16;
typedef signed short	i16;
typedef unsigned long   u32;
typedef signed long     i32;
typedef unsigned char   bool;

#define false           0
#define true            1

#define STRLLEN(x)      (sizeof(x) - 1)


#endif//STM_TYPE_H