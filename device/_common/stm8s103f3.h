#ifndef STM8S103F3_DEF
#define STM8S103F3_DEF

#include <type.h>

// Interrupt codes
#define IRQ_TOP_LVL					0x00
#define IRQ_WAKE_UP					0x01
#define IRQ_CLOCK					0x02
#define IRQ_EXTI_A					0x03
#define IRQ_EXTI_B					0x04
#define IRQ_EXTI_C					0x05
#define IRQ_EXTI_D					0x06
#define IRQ_EXTI_E					0x07
#define IRQ_SPI_END					0x0A
#define IRQ_TIM1_OVERFLOW			0x0B
#define IRQ_TIM1_COMCAP				0x0C
#define IRQ_TIM2_OVERFLOW			0x0D
#define IRQ_TIM2_COMCAP				0x0E
#define IRQ_UART1_TX_C				0x11
#define IRQ_UART1_RX_F				0x12
#define IRQ_I2C 					0x13
#define IRQ_ADC_END					0x16
#define IRQ_TIM4_OVERFLOW			0x17
#define IRQ_FLASH					0x18

// Map port registres to structures
#define PORT_A						(*(Port*) 0x5000)
#define PORT_B						(*(Port*) 0x5005)
#define PORT_C						(*(Port*) 0x500A)
#define PORT_D						(*(Port*) 0x500F)
#define PORT_E						(*(Port*) 0x5014)
#define PORT_F						(*(Port*) 0x501A)

// Uart to structure
#define UART1 						(*(UART*) 0x5230)

// GPIO Interrupt control structures
#define EXTI_CR1                    (*(ExternalInterruptControlFull*) 0x50A0)
#define EXTI_CR2                    (*(ExternalInterruptControlLim*) 0x50A1)

// #define CLK_CKDIVR                  (*(volatile u08*) 0x50C6)
// #define CLK_PCKENR1                 (*(volatile u08*) 0x50C7)

#define UART_SR_TXE (1 << 7)

#define FLASH                       (*(Flash*) 0x505A)
#define UNLOCK_DATA_KEY1            0xAE
#define UNLOCK_DATA_KEY2            0x56

#define FLASH_PROG_START            0x8000
#define FLASH_PROG_END              0x9FFF
#define FLASH_DATA_START            0x4000
#define FLASH_DATA_END              0x427F

// #define SERIAL_ADDRESS              FLASH_DATA_START
// #define SERIAL                      ((_MEM_(SERIAL_ADDRESS + 1) << 8) | _MEM_(SERIAL_ADDRESS))
#define AES_KEY_ADDRESS             (FLASH_DATA_START + 2)

#define TIM1						(*(AdvancedTimer*) 0x5250)
#define I2C1                        (*(volatile I2C*) 0x5210)
#define CLK                         (*(Clock*) 0x50C0)
#define WWDG                        (*(Watchdog*) 0x50D1)
#define ADC1                        (*(ADC*) 0x5400)
#define TIM2                        (*(GeneralTimer*) 0x5300)

#define I2C_SR1                     (*(u08*) 0x5217)
#define I2C_SR2                     (*(u08*) 0x5218)
#define I2C_SR3                     (*(u08*) 0x5219)

#define CFG_GCR                     (*(u08*) 0x7F60)
#define IWATCHDOG                   (*(IndependWatchdog*) 0x50E0)

#endif//STM8S103F3_DEF